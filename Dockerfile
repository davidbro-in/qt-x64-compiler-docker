FROM ubuntu:18.04

ARG toolchain_path
ENV env_toolchain_path=$toolchain_path

RUN apt-get update
RUN apt-get install -y make

COPY opt/"$env_toolchain_path" /opt/"$env_toolchain_path"